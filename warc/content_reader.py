class ContentReader:
  def __init__(self, stream, size):
    self.stream = stream
    self.pos = 0
    self.bytes_to_read = size

  def read(self, size = -1):
    if size == -1 or size > self.bytes_to_read:
      size = self.bytes_to_read
    if size <= 0:
      return b''

    data = self.stream.read(size)
    size = len(data)
    self.bytes_to_read -= size
    self.pos += size
    if self.bytes_to_read < 0:
      raise IOError('Read more bytes than were available')
    
    return data

  def tell(self):
    return self.pos

  def seekable(self):
    return False

  def writable(self):
    return False

  def close(self):
    self.drain()

  def drain(self):
    while self.bytes_to_read > 0:
      self.read(128 * 1024)

