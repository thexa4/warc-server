import io

from .buffer import Buffer
from .entry import Entry
from .content_reader import ContentReader
from .info import Info

class Parser:
  def __init__(self, stream):
    self.stream = stream

    self.state = ['parse-entry']
    self.current_info = None

    self.line = 1
    self.char = 0

    self.headers = {}
    self.version_buffer = Buffer(10)
    self.header_name_buffer = Buffer()
    self.header_value_buffer = Buffer()
    self.encoded_word_buffer = Buffer()
    self.encoded_word_result = Buffer()

    self.current_content_reader = None

  def __iter__(self):
    return self

  def read_char(self, allow_eof = False):
    data = self.stream.read(1)
    if len(data) == 0:
      if allow_eof:
        return None
      else:
        raise IOError(f"Unexpected end of file during parsing of {', '.join(self.state)}")
    
    if data[0] == 10:
      self.line += 1
      self.char = 0
    else:
      self.char += 1

    return data

  def __next__(self):

    def handle_parse_entry():
      char = self.read_char(True)
      if char == None:
        self.state = ['EOF']
        return

      self.version_buffer.append(char)
      if len(self.version_buffer) == 10:
        decoded_version = self.version_buffer.decode('utf-8')
        if decoded_version != "WARC/1.1\r\n" and decoded_version != "WARC/1.0\r\n":
          self.state = ['error']
          raise IOError(f"Unsupported entry version: {self.version_buffer.decode('utf-8').strip()}")

        self.state[-1] = 'parse-header-name'

    def handle_eat_newline():
      char = self.read_char()
      if char[0] != 10: #\n
        state = ', '.join(self.state)
        self.state = ['error']
        raise IOError(f"Expected newline after carriage return at line {self.line}:{self.char} during: {state}")
      
      self.state.pop()
    
    def handle_eat_empty_line():
      char = self.read_char()
      if char[0] != 13: #\r
        state = ', '.join(self.state)
        self.state = ['error']
        raise IOError(f"Expected carriage return at line {self.line}:{self.char}")
      
      self.state[-1] = 'eat-newline'

    def handle_parse_header_name():
      char = self.read_char()
      if char[0] == 13: #\r
        if len(self.header_name_buffer) != 0:
          self.state = ['error']
          raise IOError(f"Unexpected newline at {self.line}:{self.char} after: {self.header_name_buffer.decode('utf-8')}")
        self.state[-1] = 'emit'
        self.state.append('eat-newline')
        return

      if char[0] == 58: #:
        self.state[-1] = 'parse-header-value'
        return

      self.header_name_buffer.append(char)

    def handle_parse_header_value_append_encoded_word():
      self.header_value_buffer.append(self.encoded_word_result)
      self.encoded_word_result.clear()
      self.state.pop()

    def handle_parse_header_value():
      char = self.read_char()
      if char[0] == 13: #\r
        self.state[-1] = 'emit-header'
        self.state.append('eat-newline')
        return

      if char[0] == 61: #= [https://www.rfc-editor.org/rfc/rfc2047]
        self.state.append('parse-header-value-append-encoded-word')
        self.state.append('parse-encoded-word')
        return

      self.header_value_buffer.append(char)

    def handle_emit_header():
      header_name = self.header_name_buffer.decode('utf-8').title()
      header_value = self.header_value_buffer.decode('utf-8').strip()
      self.headers[header_name] = header_value

      self.header_name_buffer.clear()
      self.header_value_buffer.clear()
      self.state[-1] = 'parse-header-name'

    def handle_drain_reader():
      self.current_content_reader.drain();
      self.current_content_reader = None

      self.state.pop()

    def handle_encoded_word():
      char = self.read_char()
      if char != '?':
        self.encoded_word_result.append([ord('=')])
        self.encoded_word_result.append(char)
        self.state.pop()
        return

      print(self.stream.read(10))
      raise ValueError('unimplemented')

    handlers = {
      'parse-entry': handle_parse_entry,
      'eat-newline': handle_eat_newline,
      'eat-empty-line': handle_eat_empty_line,
      'parse-header-name': handle_parse_header_name,
      'parse-header-value-append-encoded-word': handle_parse_header_value_append_encoded_word,
      'parse-header-value': handle_parse_header_value,
      'emit-header': handle_emit_header,
      'drain-reader': handle_drain_reader,
      'parse-encoded-word': handle_encoded_word,
    }

    while self.state[-1] != 'emit':
      if self.state[-1] == 'EOF':
        raise StopIteration

      handlers[self.state[-1]]()

    record = Entry(self.version_buffer[0:-2].decode('utf-8'), self.headers, self.current_info)
    self.version_buffer.clear()
    self.current_content_reader = ContentReader(self.stream, record.content_length())

    self.state[-1] = 'parse-entry'
    self.state.append('eat-empty-line')
    self.state.append('eat-empty-line')
    self.state.append('drain-reader')
    
    if record.record_type == 'warcinfo':
      self.current_info = Info(record.version, self.headers, None)
      bytesio = io.BytesIO(self.current_content_reader.read())
      self.current_info.set_content(bytesio)
      bytesio.seek(0)
      return (self.current_info, bytesio)

    return (record, self.current_content_reader)

