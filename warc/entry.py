class Entry:
  def __init__(self, version, headers, warc_info = None):
    self.version = version
    self.warc_info = warc_info
    self.headers = headers
    self.record_type = headers['Warc-Type']

  def content_length(self):
    if 'Content-Length' not in self.headers:
      raise ValueError("No Content-Length header provided")

    return int(self.headers['Content-Length'])

