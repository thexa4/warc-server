class Buffer:
  def __init__(self, max_size = 32768):
    self.buffer = bytearray(max_size)
    self.length = 0

  def append(self,arr):
    for char in arr:
      self.buffer[self.length] = char
      self.length += 1

  def clear(self):
    self.length = 0

  def decode(self,encoding):
    return self.buffer[0:self.length].decode(encoding)

  def __len__(self):
    return self.length

  def __getitem__(self, val):
    return self.buffer[0:self.length].__getitem__(val)

