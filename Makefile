
.PHONY: test build install debian/changelog install-build-dependencies

build:
	@true

clean:
	@true

test:
	@true

warc-server.deb: debian/changelog debian/control
	@dpkg-buildpackage -us -uc
	@cp "`ls -t ../warc-server_*.deb | head -n1`" warc-server.deb

install-build-dependencies: debian/changelog debian/control
	apt instlal devscripts equivs || true
	mk-build-deps --install --remove
	apt remove --yes warc-server-deps || true

debian/changelog:
	@debian/utils/gen-changelog > "$@"

install:
	@mkdir -p ${DESTDIR}/bin
	@cp warc-server ${DESTDIR}/bin/
	@mkdir -p ${DESTDIR}/usr/share/warc-server/
	@cp -r warc ${DESTDIR}/usr/share/warc-server/warc
